FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Nicosia
ENV BUILD_DEB=true
ENV BUILD_RPM=true
ENV GENERIC_TARBALL=true
ENV REH_WEB=true

RUN apt update \
    && apt install rsync rpm curl git build-essential g++ libx11-dev libxkbfile-dev libsecret-1-dev python-is-python3 alien -y \
    && apt clean

RUN export NVM_DIR=/opt \
    && curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash \
    && . /opt/nvm.sh \
    && nvm install v16.15.1 \
    && nvm use v16.15.1 \
    && npm install -g npm@latest \
    && npm install -g yarn
