#!/bin/bash

CONTAINER_LOG=/tmp/uncoded-container.log

echo "===> Building a docker container for Uncoded. Please wait..."
docker build . | tee $CONTAINER_LOG
CONTAINER=$(tail -n1 $CONTAINER_LOG | grep 'Successfully built ')

if [ "$CONTAINER" == "" ]
then
	echo "ERROR: Docker container build failed."
	exit 1
fi

CONTAINER=$(echo $CONTAINER | cut -d' ' -f3)

echo "===> Building UNCODED! Keep tight!"

docker run --rm -it --name tmp-uncoded-builder \
	--mount type=bind,source="$(pwd)",target=/uncoded \
	$CONTAINER \
	/bin/bash -c ". /opt/nvm.sh \
	&& npm install -g yarn \
	&& cd /uncoded \
	&& ./build.sh \
	&& ./build.sh cleanup \
	&& chmod a+rw code-oss*"
