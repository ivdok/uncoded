# Uncoded

A silent VSCode - OSS build.

## Objectives
My aim is to provide a silent VSCode build that does not send telemetry, does not call home and does not do any unsolicited network connections. A text editor should just edit text instead of being a spyware. Also I don't want any rebranding and additional settings here. That is the same VSCode, but with telemetry anti-features being cut out with `sed`. Not more.

## Latest builds, hosted on https://tilde.club/
https://tilde.club/~megastallman/uncoded/

Special thanks to ~ administrators!

## Plugins installation
1. Find your plugin on https://open-vsx.org/ and download it.
2. Extensions -> Install from VSIX -> Enjoy.

Though, be careful, as most of plugins are stuffed with THEIR OWN telemetry.

## Plugins that issue suspicious external connections
- SSH FS(github.com, img.shields.io)
- reStructured Text(github.com)
- Python(Tons of connections to necrosoft domains. NOT recommended.)
- vscode-bazel(github.com, badge.buildkite.com)
- Indent One Space(current.cvd.clamav.net)
- SaltStack(current.cvd.clamav.net)
- HCL(Really too many external connections, NOT recommended.)

## Plugins whitelist.
- autoconf
- GNU Assembler Language support
- Mako
- Prettier TOML
- Reloaded C/C++

## Simple build instructions(Docker needed!)
1. Run `./build_in_docker.sh`
2. Grab your packages(DEB, RPM, Slackware/distro-agnostic tarball).

## Classic build instructions
1. Read and understand this: https://github.com/microsoft/vscode/wiki/How-to-Contribute
2. Install NodeJS, probably with NVM: `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash && nvm install --lts && nvm use --lts`
3. Install yarn: `npm install -g yarn`
4. Install other dependencies. For Ubuntu/Debian users like me - `sudo apt-get install rpm git curl build-essential g++ libx11-dev libxkbfile-dev libsecret-1-dev python-is-python3`. For RPM distros - `sudo dnf groupinstall "Development Tools" -y && sudo yum install libX11-devel.x86_64 libxkbfile-devel.x86_64 libsecret-devel fakeroot python3 -y && sudo ln -sT /usr/bin/python3 /usr/local/bin/python`
5. Run `./build.sh` to get a distro-agnostic Linux binaries in `VSCode-linux-x64` directory, or `GENERIC_TARBALL=true BUILD_DEB=true BUILD_RPM=true ./build.sh` to generate installable packages.
6. Run `./build.sh cleanup` to remove build artifacts.

## FAQ
- Does it contain VSCodium code? Yes, I've borrowed 1 script related to settings update. I appreciate VSCodium team for these ideas and initial implementation, but the project direction and goals have changed then.
- Why not VSCodium? They stopped caring about telemetry removal. They even get angry when you remind them about broken promises. It is not the priority for them any more, but it is something I need today, and something I'm trying to provide.
- Any guarantees about telemetry absence? No. I don't want to give false promises. If you find more than I've found - please make a bugreport. If some plugin sends telemetry on its own - please make a bugreport. We'll add it to our plugin blacklist.
- Operating systems supported - Linux only, but still it should be very easy to adapt to macos with `GNU sed`. Testers welcome.
- Any rebranding? No. Let's VSCode stay VSCode.
- OpenVSX store support? Not sure, will reevaluate that later. As for today - manual package download works fine.

## Known issues
- Plugin-store code is stuffed with telemetry, but fortunately - it is not used here. The protocol itself provides fields with telemetry data, that should be either excluded, or stubbed with some fake data.

## Help needed
- Please sniff telemetry activities any ways you can. For instance, I'm using `tcptracer-bpfcc`, `tcpdump udp port 53 or tcp port 53` and `tcpdump tcp port 443`. Run all those tools in 3 terminals and see if `code-oss` or `Chrome_ChildIOT` process is doing any external connections, except of localhost(127.0.0.1) tries. None expected. 
- Build system covers all my needs today, but you may improve it and adapt to your systems. Even upload builds to PPA or COPR.
